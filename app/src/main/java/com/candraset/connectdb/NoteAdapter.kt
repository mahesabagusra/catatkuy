package com.candraset.connectdb

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.candraset.connectdb.databinding.AdapterMainBinding
import com.candraset.connectdb.room.Note

class NoteAdapter(private var notes: ArrayList<Note>, private val listener: OnAdapterListener) :
    RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val binding = AdapterMainBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NoteViewHolder(binding)
    }

    override fun getItemCount() = notes.size

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note = notes[position]
        holder.binding.textTitle.text = note.title
        holder.binding.textTitle.setOnClickListener {
            listener.onRead(note)
        }
        holder.binding.iconEdit.setOnClickListener {
            listener.onUpdate(note)
        }
        holder.binding.iconDelete.setOnClickListener {
            listener.onDelete(note)
        }
    }

    inner class NoteViewHolder(val binding: AdapterMainBinding) : RecyclerView.ViewHolder(binding.root)

    fun setData(newList: List<Note>) {
        notes.clear()
        notes.addAll(newList)
        notifyDataSetChanged()
    }

    interface OnAdapterListener {
        fun onRead(note: Note)
        fun onUpdate(note: Note)
        fun onDelete(note: Note)
    }
}
