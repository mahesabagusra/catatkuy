package com.candraset.connectdb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import com.candraset.connectdb.R

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var button : Button
    private lateinit var  checkBox: CheckBox
    private lateinit var  field_pass: EditText
    private var mIsShowPass = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        getSupportActionBar()?.hide()

        button = findViewById(R.id.loginbutton)
        button.setOnClickListener(this)

        checkBox = findViewById(R.id.checkBox_pass)
        checkBox.setOnClickListener {
            mIsShowPass = !mIsShowPass
            showPassword(mIsShowPass)
        }

        field_pass = findViewById(R.id.field_pass)
    }

    private fun showPassword(isShow: Boolean) {
        if (isShow) {
            field_pass.transformationMethod = HideReturnsTransformationMethod.getInstance()
        } else {
            field_pass.transformationMethod = PasswordTransformationMethod.getInstance()
        }
        field_pass.setSelection(field_pass.text.toString().length)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.loginbutton ->{
                val intentBiasa = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intentBiasa)
            }
        }
    }
}