package com.candraset.connectdb.onboarding

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.candraset.connectdb.R

class ViewPagerAdapter(private val context: Context) : PagerAdapter() {
    private var layoutInflater: LayoutInflater? = null

    private val images = arrayOf(
        R.drawable.ic_onboarding_first,
        R.drawable.ic_onboarding_second
    )

    private val textHeads = arrayOf(
        context.getString(R.string.title_onboarding_first),
        context.getString(R.string.title_onboarding_second),
    )

    private val textContents = arrayOf(
        context.getString(R.string.desc_onboarding_first),
        context.getString(R.string.desc_onboarding_second),
    )

    override fun getCount(): Int {
        return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    @SuppressLint("InflateParams")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater!!.inflate(R.layout.item_onboarding_pager, null)

        val imageView = view.findViewById<View>(R.id.imageView) as ImageView
        imageView.setBackgroundResource(images[position])

        val textViewHead = view.findViewById<View>(R.id.textViewHead) as TextView
        textViewHead.text = textHeads[position]

        val textViewContent = view.findViewById<View>(R.id.textViewContent) as TextView
        textViewContent.text = textContents[position]

        view.setOnClickListener {

        }

        val vp = container as ViewPager
        vp.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }

}