package com.candraset.connectdb

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.candraset.connectdb.databinding.ActivitySplashBinding
import com.candraset.connectdb.onboarding.OnBoardingActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getSupportActionBar()?.hide()

        initAction()
    }

    private fun initAction() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this@SplashActivity)
        val isFirstOpen = sharedPreferences.getBoolean("isFirstOpen", false)
        GlobalScope.launch {
            delay(2000L)
            if (isFirstOpen) {
                val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this@SplashActivity, OnBoardingActivity::class.java)
                startActivity(intent)
            }
            finish()
        }
    }
}